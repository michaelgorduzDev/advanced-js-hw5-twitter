class Card {
    constructor(post, user) {
      this.post = post;
      this.user = user;
    }
  
    createCardElement() {
      const cardElement = document.createElement('div');
      cardElement.className = 'card';
  
      const titleElement = document.createElement('h3');
      titleElement.textContent = this.post.title;
  
      const contentElement = document.createElement('p');
      contentElement.textContent = this.post.body;
  
      const userElement = document.createElement('p');
      userElement.textContent = `${this.user.name} (${this.user.email})`;
  
      // Create the delete button
      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Delete';
      deleteButton.className = 'delete-button';
      deleteButton.addEventListener('click', () => {
        // Ask for confirmation before deletion
        const isConfirmed = window.confirm('Are you sure you want to delete?');
        if (isConfirmed) {
          deletePost(this.post.id);
          cardElement.remove(); 
        }
      });
  
      cardElement.appendChild(titleElement);
      cardElement.appendChild(contentElement);
      cardElement.appendChild(userElement);
      cardElement.appendChild(deleteButton); 
      userElement.className = "users";
  
      return cardElement;
    }
  }
  
  const twitterPost = document.querySelector(".feed");
  const container = document.querySelector(".container");
  
  fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
      fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(posts => {
          posts.forEach(post => {
            const postElement = createPostElement(post, users);
            twitterPost.appendChild(postElement);
          });
        })
        .catch(error => {
          console.error('Error while fetching posts:', error);
        });
    })
    .catch(error => {
      console.error('Error while fetching users:', error);
    });
  
  function createPostElement(post, users) {
    const user = users.find(u => u.id === post.userId);
    const card = new Card(post, user);
    return card.createCardElement();
  }
  
  // Function to delete a post by its ID
  function deletePost(postId) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: 'DELETE',
    })
      .then(response => response.json())
      .then(data => {
        console.log('Post deleted successfully:', data);
      })
      .catch(error => {
        console.error('Error while deleting post:', error);
      });
  }
  
  container.appendChild(twitterPost);
